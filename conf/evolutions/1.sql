# Transactions schema

# --- !Ups
create table transactions
(
	amount double null,
	transaction_type varchar(2) null,
	timestamp date default CURRENT_TIMESTAMP not null,
	id bigint auto_increment
		primary key
);


# --- !Downs

DROP TABLE Transactions;