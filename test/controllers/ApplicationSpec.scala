import org.specs2.mutable.Specification
import play.api.test._
import play.api.test.Helpers._
class ApplicationSpec extends Specification{
  "responds to the balance endpoint" in new WithApplication {
    val Some(result) =  route(app, FakeRequest(GET, "/balance"))
    status(result) must equalTo(OK)
  }
  "send 404 when route is unkown" in new WithApplication {
    val Some(result) =  route(app, FakeRequest(GET, "/unknown"))
    status(result) must equalTo(NOT_FOUND)
  }
}