name := "talabankapi"
 
version := "1.0"

lazy val `talabankapi` = (project in file(".")).enablePlugins(PlayScala)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"

resolvers += "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"
      
scalaVersion := "2.12.2"

libraryDependencies ++= Seq( jdbc , ehcache , ws , specs2 % Test , guice,  "com.typesafe.play" %% "anorm" % "2.5.3" ,
  "mysql" % "mysql-connector-java" % "5.1.30", "org.xerial" % "sqlite-jdbc" % "3.7.2", specs2 % Test)

jacoco.settings

//unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )

      