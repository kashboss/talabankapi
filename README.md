# TALA Bank Account Mini Project #

The goal of this project is to write a simple micro web service to mimic a “Bank Account”.
Through this web service, one can query about the balance, deposit money, and withdraw money. 


### Running ###
You need to download and install sbt for this application to run. Installation instructions for sbt
can be found here http://www.scala-sbt.org/download.html
Once you have sbt installed, the following at the command prompt will start up Play in development mode:

The application is set up to run off a MySql Database. The settings can be found and changed at conf/application.conf

Please run the migration or make sure the following table exists on the database

	create table transactions
	(
		amount double null,
		transaction_type varchar(2) null,
		timestamp date default CURRENT_TIMESTAMP not null,
		id bigint auto_increment
		primary key
	);

Afterwards just run the following command

	sbt run

Play will start up on the HTTP port at http://localhost:9000/. 
You don't need to deploy or reload anything -- changing any source code while the server is running will automatically recompile and hot-reload the application on the next HTTP request.

### Api Endpoints ###

	GET /balance
	POST /deposit -d {"amount": 2000}
	POST /withdraw -d {"amount": 2000}
	
### Tests ###
To run the application tests. Run the following sbt command
    
    sbt test
    
### Code Coverage ###
The project implements Jacoco Code Coverage Plugin for SBT. To generate the code coverage report run the following command
    
    sbt jacoco:cover


