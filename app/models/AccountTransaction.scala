package models

import javax.inject.Inject

import anorm._
import javax.inject.Singleton
import play.api.db.Database
import java.time.LocalDate



@Singleton
class AccountTransaction @Inject()(db: Database) {

  /**
    * Given a transaction type and date this method will query the transactions table for the number of times that
    * transaction type that was logged for the given date
    * @param transactionType
    * @param transactionDate
    * @return
    */
  def getTransactionCountByDate(transactionType: String, transactionDate: LocalDate): Int = {
    db.withConnection { implicit c =>
      val result: Int = SQL(
        """
            SELECT COALESCE(count(*),0) as total_count FROM transactions  WHERE transaction_type={transactionType} and timestamp={transactionDate}
          """
      ).on('transactionType -> transactionType, 'transactionDate -> transactionDate.toString).as(SqlParser.int("total_count").single)
      result
    }
  }

  /**
    * Given a transaction type and date this method will query the transactions table for the total amount of that
    * transaction type that was logged for the given date
    * @param transactionType
    * @param transactionDate
    * @return
    */
  def getTotalTransactionsByDate(transactionType: String, transactionDate: LocalDate): Double ={
    db.withConnection { implicit c =>
      val result: Double = SQL(
        """
            SELECT COALESCE(sum(amount), 0) as total FROM transactions WHERE transaction_type={transactionType}
            and timestamp={transactionDate}
          """
      ).on('transactionType -> transactionType, 'transactionDate -> transactionDate.toString).as(SqlParser.double("total").single)
      result
    }
  }

  /**
    * Given an amount and a transaction type this method will insert the transaction log into the transactions table
    * @param amount
    * @param transactionType
    * @return Long
    */
  def saveTransaction(amount: Double, transactionType: String): Long = {
    db.withConnection { implicit c =>
      val id: Option[Long] = SQL(
        """
           insert into transactions (amount, transaction_type, timestamp) values ({amount}, {transactionType}, {transactionDate})
          """
      ).on('amount -> amount, 'transactionType -> transactionType,
        'transactionDate -> LocalDate.now().toString).executeInsert()

      val ID = id match {
        case Some(id) => id
        case None => 0
      }
      ID
    }
  }


  /**
    * Gets the account balance by calculating total_deposits - total_withdrawals
    * @return
    */
  def getAccountBalance(): Double = {
    db.withConnection { implicit c =>
      val totalDeposits: Double = SQL(
        """
           SELECT COALESCE(sum(amount), 0) as total_deposits from transactions WHERE transaction_type = 'd'
        """
      ).as(SqlParser.double("total_deposits").single)

      val totalWithdrawals: Double = SQL(
        """
           SELECT COALESCE(sum(amount), 0) as total_withdrawals from transactions WHERE transaction_type = 'w'
        """
      ).as(SqlParser.double("total_withdrawals").single)
      totalDeposits - totalWithdrawals
    }
  }

}