package controllers

import javax.inject.{Inject, Singleton}

import models.AccountTransaction
import play.api.libs.json.{JsValue, Reads, Json}
import java.time.LocalDate

import play.api.libs.json.Json
import play.api.libs.json._
import play.api._
import play.api.mvc._

case class TransactionFormInput(amount: Double)


@Singleton
class AccountController @Inject()(cc: ControllerComponents,  accountTransaction: AccountTransaction) extends AbstractController(cc) {

  val max_withdrawal_freq_per_day = 3
  val max_deposit_freq_per_day = 4
  val max_withdrawal_limit_per_transaction = 20000
  val max_withdrawal_limit_per_day = 50000
  val max_deposit_limit_per_transaction = 40000
  val max_deposit_limit_per_day = 150000


  /**
    * Allows users to the the account balance
    *
    */
  def balance = Action {
    val total = accountTransaction.getAccountBalance()
    Ok(Json.obj("balance" -> total))
  }


  implicit val transReads = (__ \ "amount").read[Double].map(resource => TransactionFormInput(resource))

  /**
    * Allows users to deposit money into the account
    * @return
    */
  def deposit = Action(parse.json) {request =>{
    unmarshalJsValue(request) { resource: TransactionFormInput =>
        val transaction = TransactionFormInput(resource.amount)
        if(transaction.amount > max_deposit_limit_per_transaction){
          Forbidden(Json.obj("msg" -> "Exceeded Maximum Deposit Per Transaction"))
        }else if(accountTransaction.getTransactionCountByDate("d", LocalDate.now()) >= max_deposit_freq_per_day) {
          TooManyRequests(Json.obj("msg" -> "Exceeded Maximum Number of Deposits for Today"))

        }else if(accountTransaction.getTotalTransactionsByDate("d", LocalDate.now()) >= max_deposit_limit_per_day) {
          Forbidden(Json.obj("msg" -> "Exceeded Maximum  Deposits for Today"))
        }else{
          accountTransaction.saveTransaction(transaction.amount, "d")
          val newBalance = accountTransaction.getAccountBalance()
          val msg = "Deposit successful. New Balance: $" + newBalance
          Ok(Json.obj("msg" -> msg))

    }}}

  }

  /**
    * Allows users to withdraw from the account
    * @return
    */
  def withdraw = Action(parse.json) {request =>{
    unmarshalJsValue(request) { resource: TransactionFormInput =>
      val transaction = TransactionFormInput(resource.amount)
      if(transaction.amount > max_withdrawal_limit_per_transaction){
        Forbidden(Json.obj("msg" ->"Exceeded Maximum Withdrawal Per Transaction"))
      }
      else if(accountTransaction.getTransactionCountByDate("w", LocalDate.now()) >= max_withdrawal_freq_per_day) {
        TooManyRequests(Json.obj("msg" ->"Exceeded Maximum Number of Withdrawals for Today"))
      }
      else if(accountTransaction.getTotalTransactionsByDate("w", LocalDate.now()) >= max_deposit_limit_per_day) {
        Forbidden(Json.obj("msg" ->"Exceeded Maximum  Withdrawal for Today"))
      }else if(transaction.amount > accountTransaction.getAccountBalance()){
        Forbidden(Json.obj("msg" ->"Withdrawal exceeds current available balance"))
      }
      else{
        accountTransaction.saveTransaction(transaction.amount, "w")
        val newBalance = accountTransaction.getAccountBalance()
        val msg = "Withdrawal successful. New Balance: $" + newBalance
        Ok(Json.obj("msg" -> msg))

      }}}

  }


  def unmarshalJsValue[R](request: Request[JsValue])(block: R => Result)(implicit rds : Reads[R]): Result =
    request.body.validate[R](rds).fold(
      valid = block,
      invalid = e => {
        val error = e.mkString
        Logger.error(error)
        BadRequest(Json.obj("msg" -> "Invalid request expected '{\"amount\": '<Double/Integer>'}'"))
      }
    )

}
